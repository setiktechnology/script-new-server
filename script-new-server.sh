#!/bin/bash
logfile=/root/script-new-server.log
exec > $logfile 2>&1
set -v

semanage port -a -t ssh_port_t -p tcp 22022

yum install -y epel-release
yum -y update
yum -y install htop vim screen firewalld net-snmp net-snmp-utilsGeoIP GeoIP-devel GeoIP-data && geoipupdate
geoipupdate

systemctl start firewalld
systemctl enable firewalld

firewall-cmd --zone=public --add-port=22022/tcp --permanent
firewall-cmd --zone=public --add-port=161/udp --permanent
firewall-cmd --zone=public --add-port=162/udp --permanent
firewall-cmd --zone=public --add-port=3306/tcp --permanent

firewall-cmd --reload

/scripts/configure_firewall_for_cpanel

touch /etc/global_spamassassin_enable